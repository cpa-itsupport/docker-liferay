#!/bin/sh

if [ ! $JAVA_HOME ]
then
  echo JAVA_HOME not defined.
  exit
fi

if [ -n "$LIFERAY_HOME/portal-ext.properties" ]
then
  if [ ! -z "$LIFERAY_PROPERTIES_URL" ]
  then
    echo "Getting file $LIFERAY_PROPERTIES_URL..."
    curl $LIFERAY_PROPERTIES_URL -o $LIFERAY_HOME/portal-ext.properties
  fi
fi

cd liferay && ./tomcat-7.0.62/bin/catalina.sh run
